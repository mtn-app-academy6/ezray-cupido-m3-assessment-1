import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Login from './src/Login';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './src/Views/Home';
import Details from './src/Views/Details';
import Dashboard from './src/Views/Dashboard';
const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="Login" component={Login} options={{headerShown:false}} />
      <Stack.Screen name="Home" component={Home} options={{title:"Home"}}/>
      <Stack.Screen name="Detail" component={Details} options={{title:"Details"}}/>
      <Stack.Screen name="Dashboard" component={Dashboard} options={{title:"Dashboard"}}/>


      </Stack.Navigator>
    </NavigationContainer>
  
  );
} 

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ef6c00',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
