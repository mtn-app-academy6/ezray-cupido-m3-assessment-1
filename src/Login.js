//import liraries
import React, { Component } from 'react';
import { View, Dimensions, Text, StyleSheet,SafeAreaView,TextInput,TouchableOpacity} from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

// create a component
const Login = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container}>
        <Text style={{fontWeight: '100', fontSize: 35, marginBottom: 30,}}>Welcome Back</Text>
        <Text style={{ marginBottom: 10, fontSize: 20 ,}}>
            Sign in for Adventure
            </Text>
            <View style={styles.form}>
            <TextInput
          style={styles.input}
          placeholder="Email"
        
        />
        <TextInput
          style={styles.input}
         
          placeholder="Passsword"
          
        />
            </View>
       
         <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('Home')}
      >
        <Text style={{color:'white'}}>Login</Text>
      </TouchableOpacity>
      </SafeAreaView>
    );
};

// define your styles
const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
//borderWidth: 1,
        borderBottomWidth:1,
        padding: 10,
      },
      button: {
        alignItems: "center",
        backgroundColor: "#ef6c00",
        padding: 10,
        margin:10,
        borderRadius: 10
    
      },
      form:{
        margin:16,
        paddingTop:windowHeight/6 
      }
});

//make this component available to the app
export default Login;
