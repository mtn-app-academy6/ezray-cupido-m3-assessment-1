import PT from '../../assets/PT.png'
import EE from '../../assets/EE.png'
import LH from '../../assets/LH.png'
import CN from '../../assets/CN.png'


const TrailEvents= [
    {
        trail:"Pipe Track",
        level:"Beginner",
        date:"1/1/2022",
        Image:PT,
        iColor:"#ef6c00"
    },

    {
        trail:"Elephants Eye",
        level:"Beginner",
        date:"19/3/2022",
        Image:EE,
        iColor:"#ef6c00"

    },

    {
        trail:"Lions Head",
        level:"Beginner",
        date:"22/9/2022",
        Image:LH,
        iColor:"#ef6c00"

    },

    {
        trail:"Constantia Nek",
        level:"Beginner",
        date:"28/11/2022",
        Image:CN,
        iColor:"#ef6c00"

    },
    ]

    export default TrailEvents;