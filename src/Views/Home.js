//import liraries
import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, SafeAreaView, FlatList, Image,TouchableOpacity } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import TrailEvents from './Traildata';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const cardHieght = windowHeight * 0.2;
const cardWidth = windowHeight * 0.2;
// create a component
const Home = ({ navigation }) => {
    return (
        <SafeAreaView>
            <FlatList
            data={TrailEvents}
            contentInset={{padding: 12}}
             renderItem={({item}) =>{
                return(
                 <TouchableOpacity onPress={() => navigation.navigate('Detail')} style={{margin:6, height:windowHeight *0.41,backgroundColor:item.iColor, borderRadius:10}}>
                    <View style={[
                        StyleSheet.absoluteFillObject,
                        { padding:8 }
                    ]}>
                    <Text style={styles.trail}>
                        {item.trail}
                 </Text>

                 <Text style={styles.level}>
                        {item.level}
                 </Text>

                 <Text style={styles.date}>
                        {item.date}
                 </Text>

                    </View>
                    <Image style={{
                        height:140, width:200, position:'absolute', right: 0,margin:10, bottom:0
                    }}
                     source={item.Image}/>
                

                 </TouchableOpacity>  
                )
            }}
        >
            </FlatList>
        </SafeAreaView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#afb42b',
    },

    trail:{
        fontSize:24,
        fontWeight:'200',
        color:'white'

    },
    level:{
        fontSize:20,
        fontWeight:'500',
        color:'white',

    },
    date:{
        fontSize:15,
        fontWeight:'150',
        color:'white'
    },
});

//make this component available to the app
export default Home;
