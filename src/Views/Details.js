//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native';
import Dashboard from './Dashboard';

// create a component
const Details = ({navigation}) => {
    return (
        <SafeAreaView>
               <View style={styles.container}>
            <Text>Details</Text>
        </View>

        <TouchableOpacity
                style={styles.button}
                onPress={() => navigation.navigate('Dashboard')}
            >
                <Text style={{color:'black',fontWeight: '100', fontSize: 45, marginBottom: 10}}>Book Trail</Text>
            </TouchableOpacity>

        </SafeAreaView>
     
        
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default Details;
